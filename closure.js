const Counter = () => {
  let count = 0;
  return () => {
    count++;
    return count;
  };
};

let counter = Counter();
let anotherCounter = Counter();

console.log(counter());
console.log(counter());
console.log(counter());
console.log(counter());
console.log(counter());

console.log(anotherCounter());
